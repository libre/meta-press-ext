7zip:
	7z a -tzip -mx=9 \
			-xr!.git* \
			-xr!*.swp \
			-xr!*.swo \
			-x!a.load-ext_placeholder \
			-xr!LICENSE \
			-xr!README.adoc \
			-x!*.gitmodules \
			-x!Makefile \
			-x!TODO.adoc \
			-x!lost_keys* \
			-x!css/bootstrap/js \
			-x!css/bootstrap/css/bootstrap.css \
			-x!css/bootstrap/css/bootstrap-theme.css \
			-x!css/bootstrap/css/bootstrap-theme.min.css \
			-x!css/bootstrap/config.json \
			-x!css/codemirror.css \
			-x!font/AmaticSC-Regular.ttf \
			-x!font/AmaticSC-Bold.ttf \
			-x!html_locales/template.json \
			-x!html_locales/black_list.json \
			-x!img/src \
			-x!img/Meta-Press.es_title.png \
			-x!img/metapressv4-ralenti__.gif \
			-x!img/Meta-Press.es_title_from_eps.svg \
			-x!img/Meta-Press.es_title_from_eps_dark.svg \
			-x!js/update_black_list.js \
			-x!js/month_nb/month_definitions \
			-x!js/month_nb/reimplementations \
			-x!js/month_nb/package.json \
			-x!js/month_nb/test_month_nb.mjs \
			-x!js/gettext_html_auto.js/update_locales.js \
			-x!js/deps/codemirror/codemirror.js \
			-x!js/deps/codemirror/codemirror_mode_javascript.js \
		../`date +%F_%X`-meta-press-ext_7zip .

zip:
	zip -9 \
			-x *.git* \
			-x .gitmodules \
			-x *.swp* \
			-x *.swo* \
			-x a.load-ext_placeholder \
			-x *LICENSE* \
			-x *README* \
			-x Makefile \
			-x TODO.adoc \
			-x lost_keys* \
			-x css/bootstrap/js/* \
			-x css/bootstrap/css/bootstrap.css \
			-x css/bootstrap/css/bootstrap-theme.css \
			-x css/bootstrap/css/bootstrap-theme.min.css \
			-x css/bootstrap/config.json \
			-x css/codemirror.css \
			-x font/AmaticSC-Regular.ttf \
			-x font/AmaticSC-Bold.ttf \
			-x html_locales/template.json \
			-x html_locales/black_list.json \
			-x img/src/* \
			-x img/Meta-Press.es_title.png \
			-x img/metapressv4-ralenti__.gif \
			-x img/Meta-Press.es_title_from_eps.svg \
			-x img/Meta-Press.es_title_from_eps_dark.svg \
			-x js/update_black_list.js \
			-x js/month_nb/month_definitions/* \
			-x js/month_nb/reimplementations/* \
			-x js/month_nb/reimplementations/Python/* \
			-x js/month_nb/package.json \
			-x js/month_nb/test_month_nb.mjs \
			-x js/gettext_html_auto.js/update_locales.js \
			-x js/deps/codemirror/codemirror.js \
			-x js/deps/codemirror/codemirror_mode_javascript.js \
		-v -r ../`date +%F_%X`-meta-press-ext_zip .

rm_new_template:
	rm ~/Téléchargements/template.json

black_list_update:
	node js/update_black_list.js

diff_template:
	# colordiff html_locales/template.json ~/Téléchargements/template.json
	jdiff -i 2 html_locales/template.json ~/Téléchargements/template.json

renew_template: black_list_update
	rm html_locales/template.json
	echo "{}" >  html_locales/template.json
	echo "now set language to English \
	reload index.html \
	make mv_template \
	reload settings.html ; new_source.html ; welcome.html\
	make mv_template  # for each page\
	make update_locales \
	review new keys"

mv_template: diff_template
	mv ~/Téléchargements/template.json html_locales/

update_locales:
	node js/gettext_html_auto.js/update_locales.js

jsuglify:
	uglifyjs file.js > file.min.js

js-beautify:
	 js-beautify file.min.js > file.js

yui-compressor:
	yui-compressor -o file.min.js file.js

dl-compressed-dayjs-locales:
	for a in `ls /js/dayjs/locale/*.js`; do wget -O "js/dayjs/locale/`basename ${a} .js`.min.js" "https://unpkg.com/dayjs@1.8.17/locale/${a}"; done;

new_release:
	echo "check everything works also in Chromium"
	echo "check HTML against W3C validator"
	echo "update i18n"
	echo "check dependencies versions"
	echo "check IANA timezones data version"
	echo "bump version number (ex. 1.7)"
	echo "update documentation for new features"
	echo "announce the update on the website (and active the update announcer)"
	echo "upload on Mozilla (and Chrome store)"

update_timezone:
	wget "https://data.iana.org/time-zones/releases/tzdata-latest.tar.gz"
	tar xzf tzdata-latest.tar.gz
	echo "vi zone.tab"
	echo "make a valid json list of [\"continent/city\", ...]"

.PHONY: zip black_list_update mv_template update_locales
